function createCard(name, description, pictureUrl, location, start, end) {
    return `
    <div class="col h-auto">
      <div class="card shadow-lg p-3 mb-4 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        <p> ${start} - ${end}</p>
        </div>
      
        </div>
    </div>
    `;
  }

  var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
  var alertTrigger = document.getElementById('liveAlertBtn')
  
  function alert(message, type) {
    var wrapper = document.createElement('div')
    wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'
  
    alertPlaceholder.append(wrapper)
  }
  
  if (alertTrigger) {
    alertTrigger.addEventListener('click', function () {
      alert('Nice, you triggered this alert message!', 'success')
    })
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
        // throw new Error('Response not ok');
        alert("Alert, Error!!")
      } else {
        const data = await response.json();
        
        let index = 0;
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              
              const location = details.conference.location.name;
              
              const startDate = new Date(details.conference.starts).toLocaleDateString();
              const endDate = new Date(details.conference.ends).toLocaleDateString();

              const html = createCard(title, description, pictureUrl, location, startDate, endDate);
              const column = document.querySelector('.row');
              column.innerHTML += html;
            }
          }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      // console.error('error', error);
      alert('Error!!!!')
    }
  
  });
              