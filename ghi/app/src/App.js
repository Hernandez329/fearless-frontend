import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />
    <div className="container">
    {/* <Route index element={<MainPage />} /> */}
      {/* <AttendConferenceForm /> */}
      {/* <ConferenceForm /> */}
      {/* <LocationForm /> */}
      {/* <AttendeesList attendees={props.attendees}/> */}
      

    </div>
    </>
  );
}

export default App;





// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <BrowserRouter>
//       <Nav />
//       <div className="container">
//           <Routes>
//             <Route path="conferences/new" element={<ConferenceForm />} />
//             <Route path="attendees/new" element={<AttendConferenceForm />} />
//             <Route path="locations/new" element={<LocationForm />} />
//             <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
//           </Routes>
//       </div>
//     </BrowserRouter>
//   );
// }